FROM debian:jessie
RUN apt-get update
RUN apt-get install -y vim
RUN apt-get install -y tmux
RUN apt-get install -y openssh-server
RUN apt-get install -y wget
RUN apt-get install -y emacs23-nox || apt-get install -y emacs24-nox
RUN apt-get install -y git ipython


RUN mkdir -p /var/run/sshd /root/.ssh
RUN chmod 0700 /root/.ssh

RUN wget -O- https://github.com/yelizariev.keys >> /root/.ssh/authorized_keys
RUN wget -O- https://github.com/s0x90.keys >> /root/.ssh/authorized_keys

RUN mkdir -p /usr/local/src/
RUN cd /usr/local/src/
RUN git clone https://github.com/odoo/odoo.git
RUN mkdir /usr/local/src/odoo-addons -p
RUN cd /usr/local/src/odoo-addons/
RUN git clone https://github.com/odoo-russia/odoo-russia.git
RUN git clone https://github.com/yelizariev/pos-addons.git
RUN git clone https://github.com/yelizariev/website-addons.git
RUN git clone https://github.com/yelizariev/addons-yelizariev.git
RUN git clone https://github.com/OCA/web.git
RUN git clone https://github.com/OCA/server-tools.git
RUN git clone https://github.com/yelizariev/odoo-saas-tools.git
RUN cd /

#RUN wget https://raw.githubusercontent.com/daniellawrence/dot-files/master/dot-emacs
#RUN cat dot-emacs > /root/.emacs
#RUN emacs --batch --script /root/.emacs

ADD start.sh /start.sh
RUN echo 'source /start.sh' > /root/.profile

EXPOSE 22

CMD bash -c '/etc/init.d/ssh start && tail'